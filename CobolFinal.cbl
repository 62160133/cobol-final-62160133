       IDENTIFICATION DIVISION.
       PROGRAM-ID. COBOLFINAL.
       AUTHOR. CHONCHANOK.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT PROVINCE-FILE ASSIGN TO "trader3.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT PROVINCE-REPORT-FILE ASSIGN TO "trader.rpt"
              ORGANIZATION IS LINE SEQUENTIAL.              
       
       DATA DIVISION.
       FILE SECTION.
       FD PROVINCE-FILE.
       01 PROVINCE-DETAIL.
           88 END-OF-PROVINCE-FILE VALUE HIGH-VALUE.
           05 PROVINCE-ID PIC   X(4).
           05 PROVINCE-INCOME PIC  $999,999,999.
           05 PROVINCE-MEMBER   PIC   99.
           05 PROVINCE-MEMBER-INCOME PIC $999,999.
       FD  PROVINCE-REPORT-FILE.
       01  PRN-PROVINCE   PIC   X(40).

       WORKING-STORAGE SECTION.
       01  PAGE-HEADER.
           05 FILLER PIC  X(40)
              VALUE "Province Report".       
       01  COLUMN-HEADING  PIC   X(50) 
           VALUE "PROVINCE   P INCOME   MEMBER   MEMBER INCOME".
       01  PROVINCE-DETAIL-LINE.
           05 FILLER   PIC X VALUE SPACES.
           05 PRN-PROVINCE-ID   PIC   X(4).
           05 FILLER   PIC X VALUE SPACES.
           05 PRN-PROVINCE-INCOME  PIC   $999,999,999.
           05 FILLER   PIC   XX VALUE SPACES.
           05 PRN-PROVINCE-NUMBER  PIC   99.
           05 FILLER   PIC   X(4) VALUE SPACES.
           05 PRN-PROVINCE-MEMBER-INCOME PIC   $999,999.
       01  LINE-COUNT  PIC   99 VALUE ZEROES.
           88 NEW-PAGE-REQUIRED VALUE 40 THRU  99.
       
       PROCEDURE DIVISION.
       BEGIN.
           OPEN  INPUT PROVINCE-FILE 
           OPEN  OUTPUT   PROVINCE-REPORT-FILE

           PERFORM 001-READ-FILE
           PERFORM 001-PROCESS-PAGE UNTIL END-OF-PROVINCE-FILE

           CLOSE PROVINCE-FILE
           CLOSE PROVINCE-REPORT-FILE
           GOBACK
           .
       
       001-READ-FILE.
           READ PROVINCE-FILE
              AT END SET END-OF-PROVINCE-FILE TO TRUE
           END-READ
           .
       
       001-PROCESS-PAGE.
           PERFORM  001-PROCESS-HEADER
           PERFORM  001-PROCESS-DETAIL UNTIL END-OF-PROVINCE-FILE OR 
              NEW-PAGE-REQUIRED
           .


           

     





